defmodule TrixtaRoles.RolesSupervisor do
  @moduledoc """
  A supervisor that starts `RoleServer` processes dynamically.
  """

  use DynamicSupervisor

  require Logger

  alias TrixtaRoles.RoleServer

  def start_link(_arg) do
    DynamicSupervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  @doc """
  Starts a `RoleServer` process given a Role struct and supervises it.
  """
  def start_role(role) do
    child_spec = %{
      id: RoleServer,
      start: {RoleServer, :start_link, [role]},
      restart: :transient
    }

    DynamicSupervisor.start_child(__MODULE__, child_spec)
  end

  @doc """
  Stop the `RoleServer` process normally. It CAN be restarted.
  """
  def stop_role(role_id) do
    with child_pid when not is_nil(child_pid) <- RoleServer.role_pid(role_id) do
      DynamicSupervisor.terminate_child(__MODULE__, child_pid)
      Logger.info("Stopped role server process with ID '#{role_id}'.")
    end
  end

  @doc """
  Terminates the `RoleServer` process AND state normally. It CAN'T be restarted.
  """
  def terminate_role(role_id) do
    with child_pid when not is_nil(child_pid) <- RoleServer.role_pid(role_id) do
      DynamicSupervisor.terminate_child(__MODULE__, child_pid)
      Logger.info("Stopped role server process with ID '#{role_id}'.")
    end

    TrixtaStorage.delete(:roles_table, role_id)
    Logger.info("TERMINATING role server process and state with ID '#{role_id}'...")
  end

  @doc """
  List Roles
  """
  def list_role_ids() do
    list_role_ids(:processes)
  end

  def list_role_ids(:processes) do
    :global.registered_names()
    |> Enum.filter(fn {module, _name} -> module == TrixtaRoles.RoleRegistry end)
    |> Enum.reduce([], fn {_, name}, names -> [name | names] end)
    |> Enum.sort()
  end

  def list_role_ids(:all) do
    TrixtaStorage.traverse(:roles_table, fn {role_id, _role} ->
      {:continue, role_id}
    end)
    |> Enum.sort()
  end

  def list_role_ids(:inactive) do
    MapSet.difference(MapSet.new(list_role_ids(:all)), MapSet.new(list_role_ids(:processes)))
    |> MapSet.to_list()
  end

  def list_role_ids(:difference) do
    MapSet.difference(MapSet.new(list_role_ids(:all)), MapSet.new(list_role_ids(:processes)))
    |> MapSet.to_list()
  end

  def list_role_ids(:myers_difference) do
    List.myers_difference(list_role_ids(:processes), list_role_ids(:all))
  end

  @doc """
  List Role summaries
  """
  def list_role_summaries() do
    TrixtaStorage.traverse(:roles_table, fn {_role_id, role} ->
      {:continue, role}
    end)
  end

  @doc """
  Get Role details by role_id
  """
  def role_details(role_id) do
    TrixtaStorage.lookup(:roles_table, role_id)
  end

  @doc """
    Returns the details of this role's reciprocal_role
    if one is defined.

    First checks if the given role has a reciprocal_role defined.
    If not, search through all roles to check if one if them's reciprocal_role equals this role_id.
    i.e. We check both side of the relationship.
  """
  def reciprocal_role_details(role_id) do
    case role_details(role_id) do
      [{^role_id, %{:reciprocal_role => reciprocal_role_id}}] ->
        # This role exists and has a reciprocal_role defined, so we use that role.
        role_details(reciprocal_role_id)
      _ ->
        # This role doesn't have a reciprocal_role defined,
        # so we search through all roles to check if one of their reciprocal_roles equals this role_id:
        find_roles_with_reciprocal_role_id(role_id)
    end
  end

  @doc """
  Searches through all roles and returns those whose
  reciprocal_role_id is set to the given value.
  """
  def find_roles_with_reciprocal_role_id(role_id) do
    TrixtaStorage.traverse(:roles_table, fn {role_id, role_details} ->
      {:continue, {role_id, role_details}}
    end)
    |> Enum.filter(fn {_id, role_details} -> 
      is_binary(role_details.reciprocal_role) and String.downcase(role_details.reciprocal_role) === String.downcase(role_id)
    end)
  end
end
