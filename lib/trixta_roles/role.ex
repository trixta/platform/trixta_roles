defmodule TrixtaRoles.Role do
  defstruct(
    id: nil,
    name: nil,
    description: "Shiny new Role.",
    # Source describes where the role came from, e.g. a file on Gitlab. Useful for versioning.
    source: %{},
    versions: %{},
    current_state: :initialising,
    contract_actions: %{},
    contract_reactions: %{},
    agent_quota_min: 0,
    agent_quota_max: :infinity,
    agent_filter: [],
    agent_permissions: %{},
    unrestricted_access: false,
    # reciprocal_role is used if we want to map incoming reactions on this role to actions on some other complementary role.
    # For example in the case of "Core" and "Organization", an incoming reaction on the 'organization' topic might be forwarded
    # to a corresponding action on the 'core' role.
    reciprocal_role: nil,
    # Tags is an array of lowercase strings.
    tags: []
  )

  def summarize(role) do
    %{
      id: role.id,
      name: role.name,
      description: role.description,
      source: role.source,
      current_state: role.current_state,
      contract_actions: role.contract_actions,
      contract_reactions: role.contract_reactions,
      agent_quota_min: role.agent_quota_min,
      agent_quota_max: role.agent_quota_max,
      agent_filter: role.agent_filter,
      agent_permissions: role.agent_permissions,
      unrestricted_access: role.unrestricted_access,
      reciprocal_role: nil,
      tags: Map.get(role, :tags, [])
    }
  end

  def new(%TrixtaRoles.Role{id: nil}) do
    raise ArgumentError, message: "the argument ID is invalid"
  end

  def new(role = %TrixtaRoles.Role{}) do
    role =
      if role.name do
        role
      else
        %{role | name: TrixtaNames.generate()}
      end

    role
  end

  def update_state(role, new_state) do
    new_state =
      new_state
      |> Map.new(fn {k, v} ->
        case is_atom(k) do
          false -> {String.to_existing_atom(k), v}
          true -> {k, v}
        end
      end)

    validate(Map.merge(role, new_state))
  end

  @doc """
  Grants access for the specified agent to join this role.
  """
  def grant_agent_access(role, agent_id) do
    put_in(role, [Access.key(:agent_permissions), agent_id], %{"status" => "allowed"})
    |> validate()
  end

  @doc """
  Revokes access from the specified agent for joining this role.
  """
  def revoke_agent_access(role, agent_id) do
    role
    |> Map.put(:agent_permissions, Map.delete(role.agent_permissions, agent_id))
    |> validate()
  end

  @doc """
  Determines whether a particular agent has access to join this role.
  """
  def agent_has_access?(%{:unrestricted_access => true}, _agent_id) do
    true
  end

  def agent_has_access?(
        %{:id => role_id, :tags => tags, :agent_permissions => permissions},
        agent_id
      )
      when is_binary(agent_id) and is_map(permissions) do
    if "auth:type:role_reaction" in tags do
      if "grouped" in tags do
        # 'grouped' tag is used but no group was provided, so access is denied
        false
      else
        agent_has_access_via_flow?(role_id, tags, agent_id, nil, "auth:debug" in tags)
      end

    else
      (Map.get(permissions, agent_id, %{"status" => "denied"})
       |> Map.get("status")) in [:allowed, "allowed"]
    end
  end

  def agent_has_access?(_role, _agent_id) do
    false
  end

  @doc """
  Determines whether a particular agent has access to join this role group.
  """
  defp agent_has_access_via_flow?(role_id, tags, agent_id, group_id, debug) do
    auth_role_id =
      Enum.find(tags, fn tag -> String.starts_with?(tag, "auth:role:") end)
      |> String.split(":")
      |> List.last()

    auth_reaction_id =
      Enum.find(tags, fn tag -> String.starts_with?(tag, "auth:reaction:") end)
      |> String.split(":")
      |> List.last()

    payload = %{
      "role_id" => role_id,
      "agent_id" => agent_id,
      "grouped" => if group_id == nil do false else true end,
      "group_id" => group_id
    }

    {:ok, %{result: [reply, _updated_details]}} =
      TrixtaSpaceWeb.SpaceChannel.run_flow_by_reaction(
        auth_reaction_id,
        if debug do
          %{
            "debug" => true,
            "inspect" => false,
            "slowdown" => 0,
            "debug_broadcast" => %{
              "role" => "trixta_app_user"
            },
            "action_payload" => payload
          }
        else
          payload
        end,
        auth_role_id,
        agent_id
      )

    reply
  end

  # TODO: Can validate any whole or partial new role state
  # **Validation needs to happen here**
  defp validate(new_state) do
    {:ok, new_state}
    # valid? = {:ok, new_state}

    # case valid? do
    #   {:ok, new_state} ->
    #     {:ok, new_state}

    #   {:error, messages} ->
    #     {:error, messages}
    # end
  end
end
