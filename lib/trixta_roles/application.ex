defmodule TrixtaRoles.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: TrixtaRoles.Worker.start_link(arg)
      # {TrixtaRoles.Worker, arg},
      TrixtaRoles.RolesSupervisor
    ]

    TrixtaStorage.open_table(:trixta_roles, :roles_table)

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: TrixtaRoles.Supervisor]
    ret = Supervisor.start_link(children, opts)

    # load any existing roles
    TrixtaStorage.traverse(:roles_table, fn {_role_id, role} ->
      case TrixtaRoles.start_role(role) do
        {:error, {:already_started, _pid}} -> :continue
        {:error, {:unknown_error}, _} -> :continue
        {:ok, _pid, _role} -> :continue
      end
    end)

    ret
  end
end
