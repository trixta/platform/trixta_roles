defmodule TrixtaRoles.RoleVersionManager do
  @moduledoc """
  Handles the compiling and deleting of role modules for the various versions of a role.
  """
  require Logger
  alias TrixtaRoles.{RolesSupervisor, RoleServer}

  @doc """
  If no role exists for the given role_id, start one with the given version_definition as initial version.
  If the role already exists, overwrite its latest version with the given version_definition.
  """
  def start_role_or_overwrite_latest_version(role_id, version_definition)
      when is_binary(role_id) do
    case RolesSupervisor.role_details(role_id) do
      [{^role_id, %TrixtaRoles.Role{} = _existing_role_definition}] ->
        # The role already exists.
        # First overwrite any top-level fields (outside of the versions) that we may want updated:
        updated_root_fields = Map.delete(version_definition, :versions)
        RoleServer.update_state(role_id, updated_root_fields)

        # Now overwrite the latest version with the definition provided, or create a first version if none exists.
        overwrite_latest_version(role_id, version_definition)

      _ ->
        # Role does not exist yet. Start a new one.
        case append_role_version(nil, version_definition)
             |> TrixtaRoles.start_role() do
          {:ok, _, _role_result} ->
            :started_new_role

          err ->
            Logger.error("Could not start new role called #{role_id} . Details: #{inspect(err)}")
            {:error, err}
        end
    end
  end

  @doc """
  Returns a tuple of {role_struct, latest_version_struct},
  where latest_version_struct is the latest RoleVersion by created_timestamp.

  If the role is not found, :role_not_found is returned.
  If no versions are found, {role_struct, nil} is returned.
  """
  def get_latest_version(role_id) when is_binary(role_id) do
    case RolesSupervisor.role_details(role_id) do
      [{^role_id, %TrixtaRoles.Role{} = role_struct}] ->
        {role_struct, get_latest_version(role_struct)}

      [_first | _rest] ->
        :multiple_roles_found

      _ ->
        :role_not_found
    end
  end

  @doc """
  Returns the latest RoleVersion of a given role by created_timestamp.
  """
  def get_latest_version(%TrixtaRoles.Role{:versions => versions} = _role_struct) do
    case versions
         |> Map.values()
         |> Enum.max_by(fn vsn -> vsn.created_timestamp end, &>=/2, fn -> nil end) do
      %TrixtaRoles.RoleVersion{} = role_version ->
        role_version

      _ ->
        :no_role_versions_found
    end
  end

  @doc """
  If the role definition is in legacy format without versions explicitly defined,
  this function converts it, using the top-level steps as the first version.
  """
  def get_latest_version(role) do
    [role]
    |> assemble_role_versions()
    |> get_latest_version()
  end

  def get_latest_version_steps(role_id) do
    TrixtaRoles.RoleVersionManager.get_latest_version(role_id) |> elem(1) |> Map.get(:steps)
  end

  def get_role_version(role_id, version_name) do
    with [{_role_name, %TrixtaRoles.Role{} = role_definition} | _rest] <-
           RolesSupervisor.role_details(role_id),
         %{:steps => _} = role_version <- role_definition.versions |> Map.get(version_name) do
      {:ok, role_definition, role_version}
    else
      [] -> :role_not_found
      nil -> :role_version_not_found
    end
  end

  @doc """
  Sets the given role version on the role struct in space state.
  This is used, for e.g., when we want to pre-load a specific version of a role from Gitlab
  and load it into space state for compilation.
  """
  def set_role_version(%TrixtaRoles.Role{} = role, version_definition) do
    updated_role = Map.replace(role, :description, version_definition.description) 
      |> Map.replace(:contract_actions, version_definition.contract_actions) 
      |> Map.replace(:contract_reactions, version_definition.contract_reactions)
      |> Map.replace(:agent_filter, version_definition.agent_filter) 
      |> Map.replace(:unrestricted_access, version_definition.unrestricted_access) 
    
    RoleServer.update_state(role.id, updated_role)
  end

  def set_role_version(role_id, role_version) do
    with [{_role_name, %TrixtaRoles.Role{} = role_definition} | _rest] <-
           RolesSupervisor.role_details(role_id) do
      set_role_version(role_definition, role_version)
    else
      [] -> :role_not_found
    end
  end

  def set_role_version(role_id, version_name, version_details) when is_binary(version_name) do
    set_role_version(role_id, Map.put(version_details, :version_name, version_name))
  end

  def set_role_version(role_id, _, version_details) do
    # Version name is not a string, so don't pass it.
    set_role_version(role_id, version_details)
  end

  def delete_all_local_versions(role_id) do
    TrixtaRoles.RoleVersionManager.get_local_role_versions(role_id)
    |> Enum.map(fn item -> Map.get(item, "version_name") end)
    |> Enum.map(fn local_role_version ->
      case TrixtaRoles.RolesSupervisor.role_details(role_id) do
        [{^role_id, %TrixtaRoles.Role{} = existing_role_definition}] ->
          definition_without_versions =
            append_role_version(
              nil,
              Map.delete(existing_role_definition, :versions)
            )
            |> Map.put(
              :source,
              Map.get(existing_role_definition, :source)
            )

          RoleServer.update_state(role_id, definition_without_versions)
          {:ok, %{}}

        _ ->
          {:error, %{"message" => "Could not find role called #{role_id}"}}
      end
    end)
  end

  @doc """
  Fetches a list of versions available for the given role as stored in space state.
  Does not look on Gitlab.
  """
  def get_local_role_versions(role_id) do
    with [{_role_name, %TrixtaRoles.Role{} = role_definition} | _rest] <-
           RolesSupervisor.role_details(role_id) do
      role_definition.versions
      |> Enum.map(fn {version_name, %TrixtaRoles.RoleVersion{} = version_definition} ->
        {:ok, role_definition, _, _} = extract_role_version(role_definition, version_name)

        %{
          "role_id" => role_id,
          "version_name" => version_name,
          "created_timestamp" => version_definition.created_timestamp,
          "version_description" => version_definition.version_description,
          "role_definition" =>
            TrixtaRoleCode.DefinitionToJsonEncodable.to_json_encodable(role_definition)
        }
      end)
    else
      _ -> {:error, :role_not_found}
    end
  end

  def role_module(%{:name => role_name}) do
    Module.concat([
      Elixir.TrixtaRoles.Compiled,
      Macro.camelize(role_name)
    ])
  end

  def role_module(role, %{:version_name => version_name} = _role_version)
      when is_binary(version_name) do
    Module.concat([
      role_module(role),
      Versions,
      Macro.camelize(version_name)
    ])
  end

  def assemble_role_versions(%TrixtaRoles.Role{:versions => versions} = role)
      when is_map(versions) and map_size(versions) > 0 do
    # This is already a role struct saved in the correctly versioned format. Doesn't need conversion.
    role
  end

  @doc """
  Takes a list of role maps with :steps at the top level,
  and assembles them into a single TrixtaRoles.Role struct
  with the versions listed under the :versions map.
  Conflicts in the main role name, etc are resolved by using the first one in the list.
  """
  def assemble_role_versions([_ | _] = versions) do
    versions
    |> Enum.reduce(nil, fn version, acc_role ->
      append_role_version(acc_role, version)
    end)
  end

  def assemble_role_versions(%{:contract_actions => _} = first_version) do
    # We have an ordinary map representing a single role version.
    # Turn it into a role struct with the given version as the first one.
    [first_version] |> assemble_role_versions()
  end

  @doc """
  Returns a single, unversioned role map with only the latest version at the top level.
  """
  def extract_role_version(role_id) when is_binary(role_id) do
    case get_latest_version(role_id) do
      {%TrixtaRoles.Role{} = role_struct, %TrixtaRoles.RoleVersion{} = latest_version_struct, _,
       _} ->
        extract_role_version(role_struct, latest_version_struct.version_name)

      _ ->
        # Role not found or multiple roles found for this id.
        nil
    end
  end

  @doc """
  Returns a single, unversioned role map with only the latest version at the top level.
  """
  def extract_role_version(%TrixtaRoles.Role{:versions => %{}} = role_struct) do
    case get_latest_version(role_struct) do
      %TrixtaRoles.RoleVersion{:version_name => latest_version_name} ->
        extract_role_version(role_struct, latest_version_name)

      err ->
        err
    end
  end

  def extract_role_version(role_id, version_name) when is_binary(role_id) do
    case RolesSupervisor.role_details(role_id) do
      [{_, role}] ->
        extract_role_version(role, version_name)

      _ ->
        Logger.error(
          "Could not extract version #{version_name} from role #{role_id} . Role not found."
        )

        {:error, :role_not_found}
    end
  end

  @doc """
  Picks a specific version from role state and returns a map with only that version,
  so that it may be saved to version control e.g. Gitlab.
  On Gitlab, versions are denoted by the file's history, and not in the contents of any individual file ref.
  """
  def extract_role_version(role, version_name) do
    case role do
      %TrixtaRoles.Role{
        :versions => %{^version_name => %TrixtaRoles.RoleVersion{:contract_actions => contract_actions, :contract_reactions => contract_reactions} = version}
      } ->
        # Flatten the structure so that the given version's fields are at the top level of the map saved on Gitlab.
        {:ok,
         %{
           id: role.id,
           name: role.name,
           description: role.description, 
           contract_actions: contract_actions, 
           contract_reactions: contract_reactions, 
           agent_filter: role.agent_filter,
           unrestricted_access: role.unrestricted_access,
           tags: role.tags
         }, version_name, version.created_timestamp}
      _ ->
        Logger.error(
          "Could not extract version #{version_name} from role. Version not found or invalid role struct: #{
            inspect(role)
          }"
        )

        {:error, :version_not_found}
    end
  end

  @doc """
  Picks a specific version from role state and returns a map with the json-encodable definition,
  the version name and the created timestamp for that version.
  """
  def extract_role_version_json_encodable(role_id, version_name) do
    case extract_role_version(role_id, version_name) do
      {:ok, %{:steps => _} = role, ^version_name, version_timestamp} ->
        {:ok,
         %{
           "role" => TrixtaRoleCode.DefinitionToJsonEncodable.to_json_encodable(role),
           "version_name" => version_name,
           "version_timestamp" => version_timestamp
         }}

      {:error, :role_not_found} ->
        {:error, :role_not_found}

      {:error, :version_not_found} ->
        {:error, :version_not_found}

      other ->
        Logger.error(
          "Could not extract and decode version #{version_name} of role #{role_id} . extract_role_version/2 returned: #{
            inspect(other)
          }"
        )

        {:error, :cannot_extract_version}
    end
  end

  # If the role has versions, overwrite the latest one.
  # If it doesn't have any versions, create a new one.
  defp overwrite_latest_version(%TrixtaRoles.Role{} = role, version_details) do
    case get_latest_version(role) do
      %TrixtaRoles.RoleVersion{} = latest_version ->
        case set_role_version(
               role,
               Map.put(version_details, :version_name, latest_version.version_name)
             ) do
          {:ok, _} ->
            # Recompile the latest version:
            RoleServer.compile_version(role.id, latest_version.version_name, true)
            {:overwrote_latest_version, latest_version.version_name}

          err ->
            Logger.error(
              "Error overwriting latest version of role #{role.id} . Details: #{inspect(err)}"
            )

            {:error, err}
        end

      _ ->
        :no_role_versions_found
    end
  end

  defp overwrite_latest_version(role_id, new_version_details) when is_binary(role_id) do
    case RolesSupervisor.role_details(role_id) do
      [{^role_id, role_struct}] ->
        overwrite_latest_version(role_struct, new_version_details)

      _ ->
        Logger.error(
          "Could not overwrite latest version of role #{inspect(role_id)} . Role not found."
        )

        {:error, :role_not_found}
    end
  end

  # The input is already a versioned role struct. We don't have to process it.
  def append_role_version(nil, %{:versions => _} = role_struct) do
    role_struct
  end

  # Initializes a new TrixtaRoles.Role struct with the given version as the first one.
  def append_role_version(nil, version) do
    version_name = role_version_name(version)

    %TrixtaRoles.Role{
      id: version.id,
      # This is the role's main name, not the individual version's name.
      name: version.name,
      # This is the role's main description, not the individual version's name.
      description: version.description,
      tags: Map.get(version, :tags, []),
      versions: %{
        version_name => role_version_struct(version |> Map.put(:version_name, version_name))
      },
    }
  end

  def append_role_version(%TrixtaRoles.Role{} = role, version) do
    version_name = role_version_name(version)

    Map.get_and_update(role, :versions, fn current_versions ->
        { current_versions, Map.put(
          current_versions || %{},
          version_name,
          role_version_struct(Map.put(version, :version_name, version_name))
        )}
      end
    )
  end

  # The version name defailts to the current UTC timestamp if none is given.
  defp role_version_name(version) do
    case version do
      %{:version_name => name} -> name
      _ -> "initial_#{UUID.uuid4(:hex)}"
    end
  end

  defp role_version_struct(%{
    :contract_actions => contract_actions,
    :contract_reactions => contract_reactions, 
    :agent_filter => agent_filter,
    :unrestricted_access => unrestricted_access
    } = version) do
    %TrixtaRoles.RoleVersion{
      version_name: version.version_name,
      version_description: Map.get(version, :version_description, nil),
      source: version |> Map.get(:source, nil),
      contract_actions: contract_actions,
      contract_reactions: contract_reactions,
      created_timestamp: Map.get(version, :created_timestamp, to_string(DateTime.utc_now())),
      should_save_to_git: Map.get(version, :should_save_to_git, false),
      agent_filter: agent_filter,
      unrestricted_access: unrestricted_access
    }
  end

  defp role_version_struct(version) do
    Logger.error("Could not build RoleVersion struct from the given input #{inspect(version)}")
    nil
  end
end
