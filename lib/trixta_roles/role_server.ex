defmodule TrixtaRoles.RoleServer do
  @moduledoc """
  A role server process that holds a `Role` struct as its state.
  """

  use GenServer

  require Logger

  alias TrixtaRoles.Role

  @backup_interval :timer.minutes(5)

  # Client (Public) Interface

  @doc """
  Spawns a new role server process registered under the given `role_id`.
  """
  def start_link(role) do
    # ensure actions are compiled
    ensure_actions_and_reactions_are_loaded(role)

    GenServer.start_link(
      __MODULE__,
      {role},
      name: via_tuple(role.id)
    )
  end

  defp ensure_actions_and_reactions_are_loaded(role) do
    if Map.has_key?(role, :contract_actions) do
      Enum.each(role.contract_actions, fn {_key, value} ->
        ensure_modules_are_loaded(value)
      end)
    end

    if Map.has_key?(role, :contract_reactions) do
      Enum.each(role.contract_reactions, fn {_key, value} ->
        ensure_modules_are_loaded(value)
      end)
    end
  end

  defp ensure_modules_are_loaded(value) do
    if Map.has_key?(value, "handler") do
      case get_in(value, ["handler", "type"]) do
        "module" ->
          String.to_atom(get_in(value, ["handler", "name"]))
          |> Code.ensure_loaded()

        _ ->
          nil
          # do nothing
      end
    end
  end

  def summarize(role_id) do
    GenServer.call(via_tuple(role_id), :summary)
  end

  def update_state(role_id, %{} = new_state) do
    ensure_actions_and_reactions_are_loaded(new_state)

    GenServer.call(via_tuple(role_id), {:update_state, new_state})
  end

  @doc """
  Grants access to the specified agent to join the specified role.
  """
  def grant_agent_access(role_id, agent_id) do
    GenServer.call(via_tuple(role_id), {:grant_agent_access, agent_id})
  end

  @doc """
  Revokes access for the specified agent from joining the specified role.
  """
  def revoke_agent_access(role_id, agent_id) do
    GenServer.call(via_tuple(role_id), {:revoke_agent_access, agent_id})
  end

  @doc """
  Determines whether the specified agent is allowed to join the specified role.
  """
  def agent_has_access?(role_id, agent_id) do
    GenServer.call(via_tuple(role_id), {:agent_has_access?, agent_id})
  end

  @doc """
  Returns a tuple used to register and lookup a role server process by role_id.
  """
  def via_tuple(role_id) when is_pid(role_id) do
    role_id
  end

  def via_tuple(role_id) do
    {:via, :global, {TrixtaRoles.RoleRegistry, role_id}}
  end

  @doc """
  Returns the `pid` of the role server process registered under the given `role_id`, or `nil` if no process is registered.
  """
  def role_pid(role_id) do
    role_id
    |> via_tuple()
    |> GenServer.whereis()
  end

  # Server Callbacks

  def init(role) do
    {role} = role
    role_id = role.id

    role =
      case TrixtaStorage.lookup(:roles_table, role.id) do
        [] ->
          new_role = Role.new(role)
          TrixtaStorage.save(:roles_table, role.id, new_role)
          new_role

        [{^role_id, role}] ->
          Logger.info("Role with ID '#{role_id}' already exists. Restarting...")
          role
      end

    Logger.info("Spawned role server process for role ID '#{role.id}'.")

    schedule_backup()
    {:ok, role}
  end

  def handle_call(:summary, _from, role) do
    {:reply, Role.summarize(role), role}
  end

  def handle_call({:update_state, new_state}, _from, role) do
    update_dets_state(role, new_state)
  end

  def handle_call({:grant_agent_access, agent_id}, _from, role) do
    {:ok, updated_role} = Role.grant_agent_access(role, agent_id)
    update_dets_state(role, updated_role)
  end

  def handle_call({:revoke_agent_access, agent_id}, _from, role) do
    {:ok, updated_role} = Role.revoke_agent_access(role, agent_id)
    update_dets_state(role, updated_role)
  end

  def handle_call({:agent_has_access?, agent_id}, _from, role) do
    {:reply, Role.agent_has_access?(role, agent_id), role}
  end

  def handle_info(:backup, role) do
    # TODO: Do backup of the state here
    # Logger.info("State for role server process with role ID '#{role.id}' would be backed up now.")

    schedule_backup()
    {:noreply, role}
  end

  def handle_info({:ssl_closed, {:sslsocket, _transport, _pids}}, role) do
    # Found that not having this was throwing errors at this time
    {:noreply, role}
  end

  defp update_dets_state(_role, %TrixtaRoles.Role{} = new_role) do
    role_id = my_role_id()
    TrixtaStorage.save(:roles_table, role_id, new_role)
    FastGlobal.put("role_#{role_id}", nil)
    {:reply, {:ok, Role.summarize(new_role)}, new_role}
  end

  defp update_dets_state(role, new_state) do
    case Role.update_state(role, new_state) do
      {:ok, %TrixtaRoles.Role{} = new_role} ->
        update_dets_state(role, new_role)

      {:error, messages} ->
        {:reply, {:error, messages}}
    end
  end

  defp my_role_id do
    ids = :global.registered_names()

    id =
      Enum.find_value(ids, fn id ->
        if :global.whereis_name(id) == self(), do: id
      end)

    {_, id} = id
    id
  end

  defp schedule_backup() do
    Process.send_after(self(), :backup, @backup_interval)
  end
end
