defmodule TrixtaRoles.RoleVersion do
  @moduledoc """
  Represents a particular version of a role.
  """
  defstruct(
    version_name: nil,
    # Version description is what gets included in the commit message when pushing the version to Gitlab etc.
    version_description: nil,
    # actions and reactions for the versioned role
    contract_actions: %{},
    contract_reactions: %{},
    # We might only want certain major versions to be saved on Gitlab.
    should_save_to_git: false,
    # Source contains details about where the role version came from (e.g. a specific ref on gitlab. By default, the system assumes this to be a Gitlab tag.)
    source: %{},
    created_timestamp: nil,
    # Agent related data for a role data
    agent_filter: [],
    # agent_permissions: %{}, -> NB: this needs to be restored from the active role, not the versioned one!
    unrestricted_access: false
  )
end
