defmodule TrixtaRoles do
  def start_role() do
    start_role(%TrixtaRoles.Role{id: UUID.uuid1()})
  end

  def start_role(%TrixtaRoles.Role{id: nil} = role) do
    role = Map.put(role, :id, UUID.uuid1())
    start_role(role)
  end

  def start_role(%TrixtaRoles.Role{} = role) do
    case TrixtaRoles.RolesSupervisor.start_role(role) do
      {:ok, pid} -> {:ok, pid, TrixtaRoles.RoleServer.summarize(pid)}
      {:error, _} = error -> error
      _ -> {:error, {:unknown_error}, role: role}
    end
  end

  # If `role` is a plain map then atomize keys and structify
  def start_role(%{} = role) do
    role =
      role
      |> Map.new(fn {k, v} ->
        case is_atom(k) do
          false -> {String.to_existing_atom(k), v}
          true -> {k, v}
        end
      end)

    start_role(struct(TrixtaRoles.Role, role))
  end

  def start_role(role_id) do
    start_role(%TrixtaRoles.Role{id: role_id})
  end

  def stop_role(role_id) do
    TrixtaRoles.RolesSupervisor.stop_role(role_id)
  end

  def terminate_role(role_id) do
    TrixtaRoles.RolesSupervisor.terminate_role(role_id)
  end
end
