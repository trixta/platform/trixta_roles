defmodule TrixtaRolesRoleServerTest do
  use ExUnit.Case
  doctest TrixtaRoles.RoleServer

  alias TrixtaRoles.RoleServer

  setup do
    role_id = UUID.uuid1()

    role = %TrixtaRoles.Role{
      id: role_id,
      name: "Genèsis",
      description: "The Genèsis Role",
      current_state: :initialising
    }

    child_spec = %{
      id: RoleServer,
      start: {RoleServer, :start_link, [role]},
      restart: :transient
    }

    {:ok, pid} = start_supervised(RoleServer, child_spec)

    agent_id = UUID.uuid1()

    [role: role, role_id: role_id, pid: pid, agent_id: agent_id]
  end

  test "can get role summary", %{role_id: id} do
    summary = RoleServer.summarize(id)

    assert summary.id != nil
    assert summary.name == "Genèsis"
    assert summary.description == "The Genèsis Role"
    assert summary.current_state == :initialising
  end

  test "can update Role given valid complete new state", %{role: role} do
    new_state = %{
      id: role.id,
      name: "Genèsis Two",
      description: "The Genèsis Two Role",
      current_state: :active
    }

    {:ok, updated_role} = RoleServer.update_state(role.id, new_state)

    assert updated_role.id == role.id
    assert updated_role.name == "Genèsis Two"
    assert updated_role.description == "The Genèsis Two Role"
    assert updated_role.current_state == :active
  end

  test "can update Role given valid partial new state", %{role: role} do
    new_state = %{
      name: "Genèsis Three",
      current_state: :waiting
    }

    {:ok, updated_role} = RoleServer.update_state(role.id, new_state)

    assert updated_role.id == role.id
    assert updated_role.name == "Genèsis Three"
    assert updated_role.description == "The Genèsis Role"
    assert updated_role.current_state == :waiting
  end

  test "can grant access to specific agent", %{role: role, agent_id: agent_id} do
    {:ok, updated_role} = RoleServer.grant_agent_access(role.id, agent_id)
    assert RoleServer.agent_has_access?(role.id, agent_id)
  end

  test "can revoke access from specific agent", %{role: role, agent_id: agent_id} do
    {:ok, updated_role} = RoleServer.revoke_agent_access(role.id, agent_id)
    refute RoleServer.agent_has_access?(role.id, agent_id)
  end
end
