defmodule TrixtaRolesRoleAuthTest do
  use ExUnit.Case
  doctest TrixtaRoles.Role

  alias TrixtaRoles.Role

  setup do
    role_id = UUID.uuid1()

    init_state = %TrixtaRoles.Role{
      id: role_id,
      name: "Genèsis",
      description: "The Genèsis Role",
      current_state: :initialising
    }

    role = Role.new(init_state)

    agent_id = UUID.uuid1()

    [role: role, agent_id: agent_id]
  end

  test "agent has no access to begin with", %{role: role, agent_id: agent_id} do
    refute Role.agent_has_access?(role, agent_id)
  end

  test "can grant access to specific agent", %{role: role, agent_id: agent_id} do
    {:ok, updated_role} = Role.grant_agent_access(role, agent_id)
    assert Role.agent_has_access?(updated_role, agent_id)
  end

  test "can revoke access to specific agent", %{role: role, agent_id: agent_id} do
    {:ok, updated_role} = Role.grant_agent_access(role, agent_id)
    assert Role.agent_has_access?(updated_role, agent_id)
    {:ok, updated_role} = Role.revoke_agent_access(role, agent_id)
    refute Role.agent_has_access?(updated_role, agent_id)
  end
end
