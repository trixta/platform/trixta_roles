defmodule TrixtaRolesRolesSupervisorTest do
  use ExUnit.Case
  doctest TrixtaRoles.RolesSupervisor

  alias TrixtaRoles.{RoleServer, RolesSupervisor}

  setup do
    id = UUID.uuid1()
    id1 = UUID.uuid1()

    role = %TrixtaRoles.Role{
      id: id,
      name: "Genèsis",
      description: "The Genèsis Role",
      current_state: :initialising
    }

    child_spec = %{
      id: RoleServer,
      start: {RoleServer, :start_link, [role]},
      restart: :transient
    }

    {:ok, pid} = start_supervised(RoleServer, child_spec)

    role1 = %TrixtaRoles.Role{
      id: id1,
      name: "Genèsis One",
      description: "The Genèsis One Role",
      current_state: :initialising
    }

    child_spec1 = %{
      id: RoleServer1,
      start: {RoleServer, :start_link, [role1]},
      restart: :transient
    }

    {:ok, pid1} = start_supervised(RoleServer, child_spec1)

    [role: role, role_id: role.id, pid: pid, role1: role1, role1_id: role1.id, pid1: pid1]
  end

  test "roles supervisor restarts role on server crash", context do
    pid = context[:pid]
    ref = Process.monitor(pid)
    Process.exit(pid, :kill)

    receive do
      {:DOWN, ^ref, :process, ^pid, :killed} ->
        :timer.sleep(1)
        assert is_pid(TrixtaRoles.RoleServer.role_pid(context[:role_id]))
    after
      1000 ->
        raise :timeout
    end
  end

  test "can list active roles", _context do
    role_ids_list = RolesSupervisor.list_role_ids()
    # assert role_ids_list == []
    assert length(role_ids_list) >= 2
  end

  test "can list inactive roles", _context do
    active_roles_list = RolesSupervisor.list_role_ids()
    assert TrixtaRoles.stop_role(hd(active_roles_list)) == :ok
    inactive_roles_list = RolesSupervisor.list_role_ids(:inactive)
    all_roles_list = RolesSupervisor.list_role_ids(:all)
    assert length(inactive_roles_list) < length(all_roles_list)
  end

  test "can list all roles with details", _context do
    roles_list = RolesSupervisor.list_role_summaries()
    assert length(roles_list) > 1

    assert Enum.any?(roles_list, fn item -> item.name == "Genèsis" end)
    assert Enum.any?(roles_list, fn item -> item.name == "Genèsis One" end)
  end
end
