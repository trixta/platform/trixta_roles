defmodule TrixtaRolesRoleTest do
  use ExUnit.Case
  doctest TrixtaRoles.Role

  alias TrixtaRoles.Role

  setup do
    id = UUID.uuid1()

    init_state = %TrixtaRoles.Role{
      id: id,
      name: "Genèsis",
      description: "The Genèsis Role",
      current_state: :initialising
    }

    role = Role.new(init_state)

    [role: role]
  end

  test "new_role returns the intial state" do
    id = UUID.uuid1()

    init_state = %TrixtaRoles.Role{
      id: id,
      name: "Genèsis",
      description: "The Genèsis Role",
      current_state: :initialising
    }

    role = Role.new(init_state)

    assert role.id == id
    assert role.name == "Genèsis"
    assert role.description == "The Genèsis Role"
    assert role.current_state == :initialising
  end

  test "new_role returns the intial state with default values" do
    id = UUID.uuid1()

    init_state = %TrixtaRoles.Role{
      id: id
    }

    role = Role.new(init_state)

    assert role.id != nil
    assert role.name =~ ~r/^[a-z]+-[a-z]+-[0-9]+/
    assert role.description =~ ~r/.+/
    assert role.current_state == :initialising
  end

  test "can get role summary", context do
    summary = Role.summarize(context[:role])

    assert summary.id != nil
    assert summary.name == "Genèsis"
    assert summary.description == "The Genèsis Role"
    assert summary.current_state == :initialising
  end

  test "can update Role given valid complete new state", %{role: role} do
    new_state = %{
      id: role.id,
      name: "Genèsis Two",
      description: "The Genèsis Two Role",
      current_state: :active
    }

    {:ok, updated_role} = Role.update_state(role, new_state)

    assert updated_role.id == role.id
    assert updated_role.name == "Genèsis Two"
    assert updated_role.description == "The Genèsis Two Role"
    assert updated_role.current_state == :active
  end

  test "can update Role given valid partial new state", %{role: role} do
    new_state = %{
      name: "Genèsis Three",
      current_state: :waiting
    }

    {:ok, updated_role} = Role.update_state(role, new_state)

    assert updated_role.id == role.id
    assert updated_role.name == "Genèsis Three"
    assert updated_role.description == "The Genèsis Role"
    assert updated_role.current_state == :waiting
  end
end
